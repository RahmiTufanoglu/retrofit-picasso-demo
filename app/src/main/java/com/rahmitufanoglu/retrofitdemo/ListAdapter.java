package com.rahmitufanoglu.retrofitdemo;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.LisrViewHolder> {

    private List<Food> mFoodList;

    public ListAdapter(List<Food> foodList) {
        mFoodList = foodList;
    }

    @Override
    public ListAdapter.LisrViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
        return new LisrViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LisrViewHolder holder, int position) {
        Food foodList = mFoodList.get(position);

        Picasso.with(holder.civAvatar.getContext())
                .load(foodList.getImages())
                .fit()
                .into(holder.civAvatar);

        holder.tvPlaceholderOne.setText(foodList.getId());
        holder.tvPlaceholderTwo.setText(foodList.getHeader());

        // Start the Activity
        holder.view.setOnClickListener(view -> {
            Context context = view.getContext();
            Intent intent = new Intent(context, SampleDetailActivity.class);
            intent.putExtra("image", foodList.getImages());
            intent.putExtra("id", foodList.getId());
            intent.putExtra("header", foodList.getHeader());
            intent.putExtra("content", foodList.getContent());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mFoodList.size();
    }

    public void setFilter(List<Food> list) {
        mFoodList = new ArrayList<>();
        mFoodList.addAll(list);
        notifyDataSetChanged();
    }

    // Set the RecyclerView content
    static class LisrViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.civ_avatar) ImageView civAvatar;
        @BindView(R.id.tv_placeholder_one) TextView tvPlaceholderOne;
        @BindView(R.id.tv_placeholder_two) TextView tvPlaceholderTwo;
        final View view;

        LisrViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }
}
